
import React, { Component } from 'react';
import Pagination from 'react-bootstrap/lib/Pagination';
import Button from 'react-bootstrap/lib/Button';
import Panel from 'react-bootstrap/lib/Panel';
import Well from 'react-bootstrap/lib/Well';
import CampaignsListItem from './campaignsListItem';

class CampaignsList extends Component {

    constructor(props) {

        super(props);
        this.state = {
            adList: props.adList,
            showCount: 10,
            pageNum: 1,
        };
        this.handleChange = this.handleChange.bind(this);

    }

    changePageNum = async(thisNum) => {

        this.setState({
            pageNum : thisNum
        });

    }

    handleChange = (e) => {

        const count = e.target.value;
        this.setState({
            showCount : count
        });
        e.preventDefault();

    }

    handleClick = (text) => {

        console.log(text)
        
    }

    render() {

        const adList = this.state.adList;
        const totalCount = adList.data.length;
        const showCount = this.state.showCount;
        const totalNum = Math.ceil(totalCount / showCount);
        const pageNum = this.state.pageNum;
        const startNum = (pageNum * showCount) - showCount + 1;
        const endNum = (pageNum * showCount > totalCount) ? totalCount : pageNum * showCount;

        const campaignList = adList.data.map(
            (adData, i) => {
                if(i >= (startNum-1) && i < endNum){
                    return (
                        <CampaignsListItem
                            key={i}
                            adData={adData}
                        />
                    )
                }
            }
        );

        return (
            <div className="col-lg-12">
                <Panel header={<span>현재 등록하신 광고 리스트입니다.</span>} >
                    <div>
                        <div className="dataTable_wrapper">
                            <div id="dataTables-example_wrapper" className="dataTables_wrapper form-inline dt-bootstrap no-footer">
                                <div className="row">
                                    <div className="col-sm-9">
                                        <div className="dataTables_length" id="dataTables-example_length">
                                            <label htmlFor={'show'}> Show
                                                <select
                                                    name="dataTables-example_length"
                                                    aria-controls="dataTables-example"
                                                    className="form-control input-sm"
                                                    id="show"
                                                    defaultValue={showCount}
                                                    onChange={this.handleChange}
                                                >
                                                    <option value="10">10</option>
                                                    <option value="25">25</option>
                                                    <option value="50">50</option>
                                                    <option value="100">100</option>
                                                </select>
                                                entries
                                            </label>
                                        </div>
                                    </div>
                                    {/*
                                    <div className="col-sm-3">
                                        <div id="dataTables-example_filter" className="dataTables_filter">
                                            <label htmlFor={'search'}>Search:
                                                <input
                                                    type="search"
                                                    className="form-control input-sm"
                                                    placeholder=""
                                                    aria-controls="dataTables-example"
                                                    id="search"
                                                />
                                            </label>
                                        </div>
                                    </div>
                                    */}
                                </div>

                                <div className="row">
                                    <div className="col-sm-12">
                                        <table
                                            className="table table-striped table-bordered table-hover dataTable no-footer"
                                            id="dataTables-example"
                                            role="grid"
                                            aria-describedby="dataTables-example_info"
                                        >
                                            <thead>
                                                <tr role="row">
                                                    <th
                                                        tabIndex="0"
                                                        aria-controls="dataTables-example"
                                                        rowSpan="1"
                                                        colSpan="1"
                                                        style={{ width: 585 }}
                                                    >
                                                        제목
                                                    </th>
                                                    <th
                                                        tabIndex="0"
                                                        aria-controls="dataTables-example"
                                                        rowSpan="1"
                                                        colSpan="1"
                                                        style={{ width: 300 }}
                                                    >
                                                        정보
                                                    </th>
                                                    <th
                                                        tabIndex="0"
                                                        aria-controls="dataTables-example"
                                                        rowSpan="1"
                                                        colSpan="1"
                                                        style={{ width: 231 }}
                                                    >
                                                        통계
                                                    </th>
                                                    <th
                                                        tabIndex="0"
                                                        aria-controls="dataTables-example"
                                                        rowSpan="1"
                                                        colSpan="1"
                                                        style={{ width: 180 }}
                                                    >
                                                        On/Off
                                                    </th>
                                                </tr>
                                            </thead>
                                            <tbody>
                                                {campaignList}
                                            </tbody>
                                        </table>
                                    </div>
                                </div>
                                <div className="row">
                                    <div className="col-sm-5">
                                        <div
                                            className="dataTables_info"
                                            id="dataTables-example_info"
                                            role="status"
                                            aria-live="polite"
                                        >
                                            Showing {startNum} to {endNum} of {totalCount} entries
                                        </div>
                                    </div>
                                    <div className="col-sm-6" >
                                        <Pagination
                                            activePage={pageNum}
                                            items={totalNum}
                                            first
                                            last
                                            prev
                                            next
                                            onSelect={(thisNum, e) => {
                                                this.changePageNum(thisNum)
                                                e.preventDefault();
                                            }}
                                        />
                                    </div>
                                    <div className="col-sm-1 pullRight" >
                                        <Button
                                            bsStyle="primary"
                                            onClick={(e) =>{
                                                this.handleClick("write")
                                                e.preventDefault();
                                            }}
                                        >Add</Button>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <Well>
                            <h4>DataTables Usage Information</h4>
                            <p>DataTables is a very flexible, advanced tables plugin for jQuery.
                            In SB Admin, we are using a specialized version of DataTables built for Bootstrap 3.
                            We have also customized the table headings to use Font Awesome icons in
                            place of images. For complete documentation on DataTables, visit their website
                            at <a target="_blank" rel="noopener noreferrer" href="https://datatables.net/">'https://datatables.net/'</a>.
                            </p>
                            <Button bsSize="large" block href="https://datatables.net/">View DataTables Documentation</Button>
                        </Well>

                    </div>
                </Panel>
            </div>
        );
    }

}

export default CampaignsList;
