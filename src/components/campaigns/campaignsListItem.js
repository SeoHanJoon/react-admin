
import React, { PropTypes } from 'react';

const CampaignsListItem = ({adData: { id, open, user_email, weight }}) => (
    <tr className="gradeA odd" role="row">
        <td className="sorting_1">{id}</td>
        <td>{user_email}</td>
        <td className="center">{open}</td>
        <td className="center">{weight}</td>
    </tr>
);

CampaignsListItem.propTypes = {
    adData: PropTypes.shape({
        id: PropTypes.number,
        open: PropTypes.number,
        user_email: PropTypes.string,
        weight:  PropTypes.number
    })
};

export default CampaignsListItem;
