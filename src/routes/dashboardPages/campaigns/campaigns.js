
import React, { PropTypes } from 'react';
import PageHeader from 'react-bootstrap/lib/PageHeader';

const title = 'Campaigns';

function displayTable(props, context) {
    context.setTitle(title);
    const campaignTable = props.campaignTable;

    return (
        <div>
            <div className="col-lg-12">
                <PageHeader>캠페인 - 광고</PageHeader>
            </div>

            {campaignTable}

        </div>

    );
}

displayTable.contextTypes = { setTitle: PropTypes.func.isRequired };

export default displayTable;
