
import React from 'react';
import Campaigns from './campaigns';

import CampaignsList from '../../../components/campaigns/campaignsList';

import * as serviceGet from '../../../services/get';

export default {

    path: '/campaigns',

    async action() {
        const adList = await serviceGet.getAd();
        const campaignTable = (
            <CampaignsList
                adList={adList}
            />
        );

        return <Campaigns campaignTable={campaignTable} />;
    },

};
