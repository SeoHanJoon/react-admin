import axios from 'axios';

export function getAd(){
    return axios.get(`http://localhost:8000/api/ad/`)
    .then(response => {
        return response;
    })
    .catch(error => {
        return error;
    });
}
